package com.drahzek;

//imported stuff
import java.io.*;
import com.drahzek.model.Animal;
import com.drahzek.model.Herbivore;
import com.drahzek.model.Carnivore;
import com.drahzek.model.Avian;
import com.drahzek.model.Mammal;
import com.drahzek.model.Reptile;
import com.drahzek.model.Wolf;
import com.drahzek.model.Iguana;
import com.drahzek.model.Parrot;

/**
 * Main class for the animal application.
 * @author Dominik Świerzyński
 */
public class Main {
    public static void main(String[] args) {

        /*welcome message */
        System.out.println("Welcome to the ZOO");

        /* default, empty array declared so the invoked methods could work as they should */
        Animal[] animals;

        /* invoked methods, described below */
        animals = readAnimalPen(new File("AnimalSourcePen"));
        printAllAnimals(animals);
        saveAnimalPen(animals, new File("/tmp/animalTestSavedFile.txt"));
        feedAnimals(animals);
        feedMeat(animals);
        feedPlants(animals);
        voiceHowl(animals);
        voiceHiss(animals);
        voiceChirp(animals);

        //corrected binary methods
        saveAnimalPenBinary("/tmp/animalTestBinarySavedFile", animals);
        Animal[] serializedAnimals = readAnimalPenBinary("/tmp/animalTestBinarySavedFile");
        printAllAnimals(serializedAnimals);

    }

    /**
     * this method prints all animals and their characteristics from the array that was read from other method
     * @param animals
     */
    private static void printAllAnimals(Animal[] animals) {
        for (Animal animal : animals) {
            /* prints out the species, name and age of the specific animal*/
            System.out.println(animal.getClass().getSimpleName() + "\n" + animal.getName() + "\n" + animal.getAge());

            /* prints out the colour of the specific animal by getting the value linked to the abstract class */
            if (animal instanceof Avian) {
                System.out.println(((Avian) animal).getFeatherColour());
            }
            else if (animal instanceof Mammal) {
                System.out.println(((Mammal) animal).getFurColour());
            }
            else if (animal instanceof Reptile) {
                System.out.println(((Reptile) animal).getScalesColour());
            }
        }
    }

    /**
     * this method saves read array to the new file
     * @param animals
     * @param file
     */
    private static void saveAnimalPen(Animal[] animals, File file) {
        try (Writer fw = new FileWriter(file)) {
            for (Animal animal : animals) {
                /* saves the species, name and age of the specific animal from the array */
                fw.write(animal.getClass().getSimpleName() + "\n");
                fw.write(animal.getName() + "\n");
                fw.write(animal.getAge() + "\n");

                /* saves the colour of the specific animal by getting the value linked
                to the abstract class */
                if (animal instanceof Avian) {
                    fw.write(((Avian) animal).getFeatherColour() + "\n");
                }
                else if (animal instanceof Mammal) {
                    fw.write(((Mammal) animal).getFurColour() + "\n");
                }
                else if (animal instanceof Reptile) {
                    fw.write(((Reptile) animal).getScalesColour() + "\n");
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * this method reads animals from the source file and returns them in the array for the further use
     * @param file
     * @return
     */
    private static Animal[] readAnimalPen(File file) {
        /* from java 7 we can autoclose streams by using the following way of using try */
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            /* reads the first line from the text file, then converts it into integer and uses it to set up
            an array of the same size as the converted value */
            String textSize = br.readLine();
            int size = Integer.parseInt(textSize);
            Animal[] animals = new Animal[size];

            /* default animal set up so the following code could "see" its existence */
            Animal animal = null;

            for (int i = 0; i < size; i++) {
                /* reads the four lines, responsible for the species, name, age and colour of
                the animal, starting from the second line of a file. Age is converted from
                string to integer and declared as a new age variable */
                String species = br.readLine();
                String name = br.readLine();
                String ageT = br.readLine();
                int age = Integer.parseInt(ageT);
                String colour = br.readLine();

                /* by getting the predetermined species value from the file, we create the right animal of
                 * a said species and set the right kind of scales/fur/feathers colour */
                switch (species) {
                    case "Iguana": {
                        animal = new Iguana();
                        ((Iguana) animal).setScalesColour(colour);
                        break;
                    }
                    case "Parrot": {
                        animal = new Parrot();
                        ((Parrot) animal).setFeatherColour(colour);
                        break;
                    }
                    case "Wolf": {
                        animal = new Wolf();
                        ((Wolf) animal).setFurColour(colour);
                        break;
                    }
                }

                /* setting up the name and age of the animal by linking the variable read by the FileReader
                to the variables declared in Animal Class */
                animal.setName(name);
                animal.setAge(age);

                /* adding animal and the linked values to the array */
                animals[i] = animal;
            }

            /* returns array for further use outside */
            return animals;
        } catch (IOException ex) {
            System.err.println(ex);
            return new Animal[0];
        }
    }

    /**
     * method responsible for feeding all animals, regardless of the type of food
     * @param animals
     */
    private static void feedAnimals(Animal[] animals) {
        System.out.println("");
        for (Animal animal : animals) {
            if (animal != null) {
                animal.eat();
            }
        }
    }

    /**
     * method responsible for feeding all carnivores
     * @param animals
     */
    private static void feedMeat(Animal[] animals) {
        System.out.println("");
        for (Animal animal : animals) {
            if (animal instanceof Carnivore) {
                ((Carnivore) animal).eatMeat();
            }
        }
    }

    /**
     * method responsible for feeding all herbivores
     * @param animals
     */
    private static void feedPlants(Animal[] animals) {
        System.out.println("");
        for (Animal animal : animals) {
            if (animal instanceof Herbivore) {
                ((Herbivore) animal).eatPlants();
            }
        }
    }

    /**
     * Method responsible for the howling of the animals capable of that.
     * Since all voices were declared for the specific species and not inside of the interfaces or upper classes,
     * we use instanceof for the specific species as well, which is not efficient if we want to add more
     * animals with similar method in the future. We can try to use obj.getClass().getMethod() inside ifs
     * to check if the object has that method but it's prone to errors inside the code structured like that
     * and it's not going to be the universal solution.
     * @param animals
     */
    private static void voiceHowl(Animal[] animals) {
        System.out.println("");
        for (Animal animal : animals) {
            if (animal instanceof Wolf) {
               ((Wolf) animal).howl();
            }
        }
    }

    /**
     * method responsible for the hissing of the animals capable of that.
     * @param animals
     */
    private static void voiceHiss(Animal[] animals) {
        System.out.println("");
        for (Animal animal : animals) {
            if (animal instanceof Iguana) {
                ((Iguana) animal).hiss();
            }
        }
    }

    /**
     * method responsible for the chirping of the animals capable of that
     * @param animals
     */
    private static void voiceChirp(Animal[] animals) {
        System.out.println("");
        for (Animal animal : animals) {
            if (animal instanceof Parrot) {
                ((Parrot) animal).chirp();
            }
        }
    }

    /**
     * method responsible for saving the array with animals to the binary file
     * in previous commit (7d3e62e) it was done by using a different, longer method
     * @param animals
     * @param file
     */
    private static void saveAnimalPenBinary(String file, Animal[] animals) {
        try (ObjectOutputStream apboos = new ObjectOutputStream(new FileOutputStream(file))) {
            System.out.println("");
            System.out.println("Binary file saved!");
            apboos.writeObject(animals);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * method responsible for reading the array saved earlier to the binary file
     * in previous commit (7d3e62e) it was done by using a different, longer method
     * @param file
     * @return
     */
    private static Animal[] readAnimalPenBinary(String file) {
        try (ObjectInputStream apbois = new ObjectInputStream(new FileInputStream(file))) {
            System.out.println("");
            System.out.println("Binary file loaded!");
            return (Animal[]) apbois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
            return new Animal[0];
        }
    }
}