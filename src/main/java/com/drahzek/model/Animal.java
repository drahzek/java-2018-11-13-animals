package com.drahzek.model;

import java.io.Serializable;

public abstract class Animal implements Serializable {

    /* private properties for the objects of this class */
    private String name;
    private int age;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    /* method responsible for the eating habits of the animals */
    public abstract void eat();

    public Animal() {
    }
}
