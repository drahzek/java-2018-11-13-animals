package com.drahzek.model;

import java.io.Serializable;

public abstract class Avian extends Animal implements Serializable {
    private String featherColour;

    public String getFeatherColour() {
        return featherColour;
    }

    public void setFeatherColour(String featherColour) {
        this.featherColour = featherColour;
    }
}
