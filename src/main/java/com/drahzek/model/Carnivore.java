package com.drahzek.model;

public interface Carnivore {
    void eatMeat();
}
