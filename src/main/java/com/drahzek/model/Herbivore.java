package com.drahzek.model;

public interface Herbivore {
    void eatPlants();
}
