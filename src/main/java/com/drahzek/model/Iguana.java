package com.drahzek.model;

import java.io.Serializable;

public class Iguana extends Reptile implements Herbivore, Serializable {

    public void hiss() {
        System.out.println(getName() + " is hissing");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " is eating something");
    }

    @Override
    public void eatPlants() {
        System.out.println(getName() + " is eating plants");
    }

    /*default creator */
    public Iguana() {
    }
}
