package com.drahzek.model;

import java.io.Serializable;

public abstract class Mammal extends Animal implements Serializable {
    private String furColour;

    public String getFurColour() {
        return furColour;
    }

    public void setFurColour(String furColour) {
        this.furColour = furColour;
    }
}
