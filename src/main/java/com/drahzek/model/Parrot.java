package com.drahzek.model;

import java.io.Serializable;

public class Parrot extends Avian implements Herbivore, Serializable {

    public void chirp() {
        System.out.println(getName() + " is chirping");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " is eating something");
    }

    @Override
    public void eatPlants() {
        System.out.println(getName() + " is eating plants");
    }

    /*default creator */
    public Parrot() {
    }
}
