package com.drahzek.model;

import java.io.Serializable;

public abstract class Reptile extends Animal implements Serializable {
    private String scalesColour;

    public String getScalesColour() {
        return scalesColour;
    }

    public void setScalesColour(String scalesColour) {
        this.scalesColour = scalesColour;
    }
}
