package com.drahzek.model;

import java.io.Serializable;

public class Wolf extends Mammal implements Carnivore, Serializable {

    public void howl() {
        System.out.println(getName() + " is howling");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " is eating something");
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + " is eating meat");
    }

    /*default creator */
    public Wolf() {
    }
}
